public class Group {
    public int id;
    public String name;

    public Group(){
    }

    @Override
    public String toString() {
        return String.format("#" + id + " - " + name);
    }
}

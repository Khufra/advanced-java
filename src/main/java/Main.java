import java.sql.*;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        String url = "jdbc:postgresql://127.0.0.1:5432/postgres";
        String user = "davide";
        String pass = "jw8s0F4";
        Connection conn = null;
        try {
            Class.forName("org.postgresql.Driver");

            conn = DriverManager.getConnection(url,user,pass);
            Statement stat = conn.createStatement();

            ResultSet group_result = stat.executeQuery("select * from groups");

            Group group = new Group();
            while(group_result.next()){
                group.id = group_result.getInt("id");
                group.name = group_result.getString("name");
                System.out.println(group.toString());
            }

            ResultSet student_result = stat.executeQuery("select * from student");

            Student student = new Student();
            while(student_result.next()){
                student.id = student_result.getInt("id");
                student.name = student_result.getString("name");
                student.phone = student_result.getInt("phone");
                student.group_id = student_result.getInt("group_id");
                System.out.println(student.toString());
            }

            Group group_sout = new Group();
            Student student_out = new Student();
            ResultSet group_res = stat.executeQuery("select * from groups");
            while(group_res.next()){
                System.out.println(group_res.getString("name") + " - " + group_res.getInt("id") + ":");
                group_sout.id = group_res.getInt("id");
                group_sout.name = group_res.getString("name");
                ResultSet student_res = stat.executeQuery("select * from student");

                while(student_res.next()){
                    student_out.id = student_res.getInt("id");
                    student_out.name = student_res.getString("name");
                    student_out.phone = student_res.getInt("phone");
                    student_out.group_id = student_res.getInt("group_id");

                    if(group_sout.id == student_out.group_id) {
                        System.out.println(student_out.toString());
                    }
                }
            }


        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally{
            if( conn != null){
                try{
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

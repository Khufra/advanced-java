public class Student {
        public int id ;
        public String name;
        public int phone;
        public int group_id;

        public Student(){

        }

        @Override
        public String toString() {
                return String.format("#" + id + " - " + name + " " + phone + " " + " - " + group_id);
        }
}
